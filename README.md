![Build Status](https://gitlab.com/Myrik/sociodoc/badges/master/build.svg)

---

Example plain HTML site using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

https://myrik.gitlab.io/sociodoc/